package aula;


public class DateTime {
    private Time time;
    private int dia, mes, ano;
    
    
    public DateTime(int dia, int mes, int ano, 
                    int horas, int minutos, int segundos) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.time = new Time(horas, minutos, segundos);
    }
    
    
    public String toString() {
        return String.format("%02d/%02d/%04d %s", dia, mes, ano, time.toString());
    }
    
}
